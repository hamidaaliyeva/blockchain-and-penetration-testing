package com.company;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Block> blockchain = new ArrayList<>();
//        Block block = new Block("Blockchain Data Name",
//                "91eab1f7c0dba6d18a37e489132a909f2ee03bffee453190bc312ce1dbc3d3d1",
//                new Date().getTime());
        String previousHash = "91eab1f7c0dba6d18a37e489132a909f2ee03bffee453190bc312ce1dbc3d3d1";
        while (blockchain.size() < 10){
            Block block = new Block("Blockchain Data Name",
//                    previousHash,
                    blockchain.size() == 0 ? null : blockchain.get(blockchain.size() - 1).getHash(),
                    new Date().getTime());
            previousHash = block.getHash();
            blockchain.add(block);
            System.out.println(block);
        }
        int prefix = 4;
        String prefixString = new String(new char[prefix]).replace('\0', '0');
    }
}